from django.urls import path
from django.contrib.auth import views as auth_views

from myapp import views as myapp_view

app_name = "myapp"

urlpatterns = [
    path('', myapp_view.home, name='home'),
    path('login/', auth_views.LoginView.as_view(template_name='login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(next_page='login'), name='logout'),
    path('list/', myapp_view.list, name='list'),
    path('download/', myapp_view.download, name='download'),
    path('home/', myapp_view.home, name='home'),
]
