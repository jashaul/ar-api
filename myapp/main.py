import nltk

# nltk.download('stopwords')
# nltk.download('wordnet')
# nltk.download('averaged_perceptron_tagger')

import aylien_news_api
from aylien_news_api.rest import ApiException
import gensim
import pandas as pd
import nltk
from sklearn.feature_extraction.text import TfidfVectorizer
from collections import Counter
import string

from gensim import corpora
from gensim.models import TfidfModel
from gensim.models import LsiModel
from gensim.similarities import MatrixSimilarity

# sys.path.append('/home/gyan/anaconda2/lib/python2.7/site-packages')

from nltk.corpus import wordnet


def get_wordnet_pos(treebank_tag):
    """
        A function which obtains the wordnet pos for the nltk pos tags
    """
    if treebank_tag.startswith('J'):
        return wordnet.ADJ
    elif treebank_tag.startswith('V'):
        return wordnet.VERB
    elif treebank_tag.startswith('N'):
        return wordnet.NOUN
    elif treebank_tag.startswith('R'):
        return wordnet.ADV
    else:
        return ''


translator = lambda x: ''.join(xx if xx not in string.punctuation else ' ' for xx in x)
stopwords_list = [translator(x) for x in nltk.corpus.stopwords.words('english')]
lemmatizer = nltk.stem.WordNetLemmatizer()
tokenizer = nltk.tokenize.RegexpTokenizer('\w+')
lemmatized = lambda x: [
    lemmatizer.lemmatize(xx, get_wordnet_pos(pos)) if get_wordnet_pos(pos) else lemmatizer.lemmatize(xx) for xx, pos in
    nltk.pos_tag(x.lower().split())]
cleaned_file = lambda x: [xx for xx in lemmatized(x) if translator(xx) not in stopwords_list]
return_nouns = lambda x: [filter_word for filter_word, filter_tag in nltk.pos_tag(x) if 'NN' in filter_tag]
decoder = lambda sent: ''.join(char for char in sent if ord(char) < 128)


def get_keywords_for_article(article_to_estimate):
    """
  A function that cleans the input article based on some pre processing steps and obtains all the important keywords
  that holds the meaning of the entire article
  Once the keywords are extracted , the most important keywords are determined by their significance and contribution
  to the total meaning of the sentence
  Returns keywords with different significance scores from different metrics
  Data types : input - str
  Output - Dictionary
    """

    try:
        article_to_estimate = decoder(article_to_estimate)
        cleaned_article = cleaned_file(article_to_estimate)
        gensim_keywords = gensim.summarization.keywords(' '.join(cleaned_article)).split('\n')
        if gensim_keywords != ['']:
            all_sentences = [x for x in filter(lambda x: x, ' '.join(cleaned_article).split('\n'))]
            vect = TfidfVectorizer(ngram_range=(1, 3), stop_words=nltk.corpus.stopwords.words('english'))
            sentences_tfidf = vect.fit_transform(all_sentences)
            df_tfidf = pd.DataFrame(sentences_tfidf.toarray(), columns=vect.get_feature_names())
            keywords_from_tfidf = list(df_tfidf.mean().sort_values(ascending=False)[:10].index)
            keywords_from_tf = [x for x, y in Counter(cleaned_article).most_common(5)]
            return {'keywords_from_gensim': return_nouns(gensim_keywords),
                    'keywords_from_tfidf': return_nouns(keywords_from_tfidf), 'keywords_from_tf': keywords_from_tf}
        else:
            print('There was no keywords extracted  ')
            return {'keywords_from_gensim': [], 'keywords_from_tfidf': [], 'keywords_from_tf': []}
    except Exception as e:
        print('Excepted ', e)
        return {'keywords_from_gensim': [], 'keywords_from_tfidf': [], 'keywords_from_tf': []}


# Configure API key authorization: app_id
aylien_news_api.configuration.api_key['X-AYLIEN-NewsAPI-Application-ID'] = 'f3c73065'
# Configure API key authorization: app_key
aylien_news_api.configuration.api_key[
    'X-AYLIEN-NewsAPI-Application-Key'] = '981430a79252c0344c8e6d45f43a247e'

# create an instance of the API class
api_instance = aylien_news_api.DefaultApi()


def get_news_aylien(query_term):
    """
    Takes a search term and return a dictionary of the news results
    The output has got the results from the api as a dictionary with different parameters from the API
    Input Type : str
    Output Type : dict
    """

    opts = {
        'title': query_term,
        'sort_by': 'social_shares_count.facebook',
        'language': ['en'],
        'per_page': 100,
        'published_at_start': 'NOW-180DAYS',
        'published_at_end': 'NOW'
    }
    print('query term to api  ', query_term)
    try:
        # List stories
        api_response = api_instance.list_stories_with_http_info(**opts)
        print('API called successfully.')
    except ApiException as e:
        api_response = {}
        print("Exception when calling DefaultApi->list_stories: %sn" % e)
        return "No results "
    try:
        api_results_dict = api_response[0].to_dict()['stories']
        return [{k: decoder(v) if type(v) == str else v for k, v in api_result.items()} for api_result in
                api_results_dict]
    except Exception as e:
        print('Excepted ', e)
        return []


def get_facebook_shares(api_res):
    """
    Takes the results from the API call and returns the facebook shares and the
    corresponding keywords
    """
    return [{x['social_shares_count']['facebook'][0]['count']: x['keywords'] for x in api_res}]


def get_sim_res(data):
    """
    Takes an input article and estimates the keywords for those article . Obtains the relevant news articles from the
    api for the extracted keywords
    Input - str
    Output - News results as  a dict
    """
    keywords = get_keywords_for_article(data)
    data = ''.join(x for x in data if ord(x) < 128)
    search_term = set([xx for x in keywords['keywords_from_gensim']
                       for xx in x.split()]).intersection(set(keywords['keywords_from_tfidf']))
    search_phrase = ' '.join(search_term)
    results = get_news_aylien(search_phrase)
    return results


def return_lsi_similarity_prob(results, article_to_estimate):
    """
    For the similar results extracted from aylien api and the input article , a LSI similarity matrix is built for all
    the results extracted
    The input blog is compared against the articles extracted and a sim score is given from -1 to 1
    in the vec_lsi variable
    Input - list of results and str
    Output - list of LSI sim index
    """
    tokenizer_words = nltk.tokenize.RegexpTokenizer('[a-zA-Z]+')
    lemmatized_words = lambda x: [
        lemmatizer.lemmatize(xx, get_wordnet_pos(pos)) if get_wordnet_pos(pos) else lemmatizer.lemmatize(xx) for xx, pos
        in nltk.pos_tag(tokenizer_words.tokenize(x.lower()))]
    clean_words = lambda x: [xx for xx in lemmatized_words(translator(decoder(x))) if xx not in stopwords_list]
    # print("6" , results)
    try:
        all_documents = [clean_words(x['body']) for x in results]
        dictionary = corpora.Dictionary(all_documents)
        corpus_gensim = [dictionary.doc2bow(doc) for doc in all_documents]
        tfidf = TfidfModel(corpus_gensim)
        corpus_tfidf = tfidf[corpus_gensim]
        lsi = LsiModel(corpus_tfidf, id2word=dictionary, num_topics=200)
        lsi_index = MatrixSimilarity(lsi[corpus_tfidf])
        new_doc_vec = dictionary.doc2bow(clean_words(article_to_estimate))
        vec_lsi = lsi[new_doc_vec]
        sims = lsi_index[vec_lsi]
        return list(sims)
    except Exception as e:
        print(e)
        return []


def return_entities_keywords_percent(results, article_to_estimate):
    """
    For the similar results extracted from aylien api and the input article , the keywords and entities returned from
    the api is obtained
    The results are said to be more similar to the article or blog if they share a few keywords or entities
    Variables explained - l : match percentage of the entities and the keywords between the articles from news api
    and input article
    common_ents_kws - list of keywords and entities that are shared between the input article and the different
    articles from the aylien api
    different_ents_kws - list of keywords and entities that are present only in the news articles from the aylien
    api but not present in the input article
    Input - list , str
    Output - list , list , list
    """
    l = []
    common_ents_kws = []
    different_ents_kws = []
    if not results or results == 'No results ':
        return [], [], []
    for ind_res in results:
        entities_to_check = [xx['text'].lower() for x in ind_res['entities'].values() for xx in x]
        keywords_to_check = ind_res['keywords']
        entities_keywords_to_check = set(entities_to_check).union(set(keywords_to_check))
        common_entities_keywords = [ent for ent in entities_keywords_to_check if ent in article_to_estimate]
        entities_kws_match_percent = len(common_entities_keywords) / float(len(entities_keywords_to_check))
        common_ents_kws.append(set(common_entities_keywords))
        different_ents_kws.append(entities_keywords_to_check.difference(set(common_entities_keywords)))
        l.append(entities_kws_match_percent)
    return l, common_ents_kws, different_ents_kws


def get_final_similarity(article_to_estimate, results, sim_weight, enty_weight, max_sim=0.3):
    """
    The final module that takes in any article to estimate as a string and has 2 weights - similarty weight and the
    entity weight which becomes the weights for finding the final similarity between the input article
    and the articles from the api
    The final result has all the news articles from the aylien api which is sorted in the reverse order based on the
    aggregated (weighted mean) of different similarity scores
    Input - str , float , float
    Output : list
    """

    lsi_sim = return_lsi_similarity_prob(results, article_to_estimate)
    ents_kws_sim, common_ents_kws, different_ents_kws = return_entities_keywords_percent(results, article_to_estimate)
    sim_score_filtered_list = []
    for lsi_s, ents_s, common_e_k, diff_e_k, res in zip(lsi_sim, ents_kws_sim, common_ents_kws, different_ents_kws,
                                                        results):
        final_score = lsi_s * sim_weight + ents_s * enty_weight
        if final_score >= max_sim:
            sim_score_filtered_list.append(
                (res['links']['permalink'], res['body'], res['hashtags'], common_e_k, diff_e_k,
                 {k: max(v, key=lambda x: x.keys())['count'] for k, v in res['social_shares_count'].items() if v},
                 final_score))
    if not sim_score_filtered_list:
        return []
    max_score = max(sim_score_filtered_list, key=lambda x: x[-1])[-1]
    filtered_list = filter(lambda x: x[-1] > 0.8 * max_score, sim_score_filtered_list)
    return sorted(filtered_list, reverse=True, key=lambda x: x[-1])
