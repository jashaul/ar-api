from django.contrib.auth.models import User
from django.db import models


class Doc(models.Model):
    url = models.CharField(max_length=1000, blank=True)
    body = models.CharField(max_length=5000, blank=True)
    hashtags = models.CharField(max_length=1000, blank=True)
    comkeywords = models.CharField(max_length=1000, blank=True)
    diffkeywords = models.CharField(max_length=1000, blank=True)
    socialsharesReddit = models.CharField(max_length=1000, blank=True)
    socialsharesGoogle = models.CharField(max_length=1000, blank=True)
    socialsharesFb = models.CharField(max_length=1000, blank=True)
    socialsharesLinkedin = models.CharField(max_length=1000, blank=True)
    simscore = models.CharField(max_length=100, blank=True)
    firstLine = models.CharField(max_length=1000, blank=True)
    socialshares_temp = models.CharField(max_length=1000, blank=True)

    class Meta:
        ordering = ['-simscore']


class UserProfile(models.Model):
    # This field is required.
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.username
