import os
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse, Http404
from django.shortcuts import render

from .models import Doc
from .driver import *


@login_required
def home(request):
    return render(request, 'main.html')


@login_required
def list(request):
    if request.method == 'POST' and request.FILES['myfile']:
        myfile = request.FILES['myfile']
        fs = FileSystemStorage()
        fs.save(myfile.name, myfile)
        uploaded_file_url = fs.path(myfile.name)
        filtered_res = drive(uploaded_file_url)
        entries = []
        for e in filtered_res.T.to_dict().values():
            entries.append(Doc(**e))
        Doc.objects.all().delete()
        Doc.objects.bulk_create(entries)
        documents = Doc.objects.all()
        return render(request, 'dashboard.html',
                      {
                          'documents': documents,
                          'output_filename': settings.OUTPUT_FILENAME
                      }
                      )

    return render(request, 'main.html')


def download(request):
    if request.method == 'GET':
        file_path = os.path.join(settings.MEDIA_ROOT, settings.OUTPUT_FILENAME + '.csv')
        if os.path.exists(file_path):
            with open(file_path, 'rb') as fh:
                response = HttpResponse(fh.read(), content_type="application/vnd.ms-excel")
                response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
                return response
    raise Http404
