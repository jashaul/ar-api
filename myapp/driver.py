import myapp.main as main
from docx import Document
import pandas as pd
import sys


def drive(file_path):
    """
A function that 
1. takes in any news articles as a flat file (txt or docx file ) 
2. reads it into a variable 
3. Finds all the predominant keywords from the document 
4. Returns all the relevant articles from the aylien news api 
5. Finds similarity between the extracted articles and the input document 
6. Estimates the social share estimate from the shares of the similar articles extracted 
7. Returns to a data frame which has the URL , body of the article in a summarized way , social shares from different
    sources as reddit , facebook , google , linkedin and similarity score
8. Dumps the dataframe to an output file 
    """
    if file_path.endswith('.txt'):
        with open(file_path, 'r', encoding="ISO-8859-1") as f:
            data = f.read().replace('\n', '')
    elif file_path.endswith('.docx'):
        document = Document(file_path)
        data = '\n'.join([para.text for para in document.paragraphs])
    else:
        df = pd.DataFrame(columns=['url', 'body', 'hashtags', 'comkeywords', 'diffkeywords', 'simscore', 'firstLine',
                                   'socialsharesReddit', 'socialsharesGoogle', 'socialsharesFb',
                                   'socialsharesLinkedin'])
        df['firstLine'] = ['Could not process your file. Please upload txt or docx file ']
        df = df.fillna(0)
        df.to_csv('media/all_results.csv', index=False, encoding='utf8')
        return df  # Not  a valid input
    data = ''.join(x for x in data if ord(x) < 128)
    results = main.get_sim_res(data)
    filtered_res = main.get_final_similarity(data, results, sim_weight=0.6,
                                             enty_weight=0.4)
    if not filtered_res:
        # When "No similar article was found "
        print("inside no filte")
        df = pd.DataFrame(columns=['url', 'body', 'hashtags', 'comkeywords', 'diffkeywords', 'simscore', 'firstLine',
                                   'socialsharesReddit', 'socialsharesGoogle', 'socialsharesFb',
                                   'socialsharesLinkedin'])
        df['firstLine'] = ['No similar news articles obtained ']
        df = df.fillna(0)
        df.to_csv('media/all_results.csv', index=False, encoding='utf8')
        print(df)
        return df
    try:
        # creates a dataframe from the filtered result
        df = pd.DataFrame(list(zip(*filtered_res))).T
        df.columns = ['url', 'body', 'hashtags', 'comkeywords', 'diffkeywords', 'socialshares', 'simscore']
        df['firstLine'] = df['body'].apply(lambda x: x.split('.')[0])
        df = pd.concat([df.drop(['socialshares'], axis=1), df['socialshares'].apply(pd.Series)], axis=1)
        df = df.rename(
            columns={'reddit': 'socialsharesReddit', 'google_plus': 'socialsharesGoogle', 'facebook': 'socialsharesFb',
                     'linkedin': 'socialsharesLinkedin'})
        df.to_csv('media/all_results.csv', index=False, encoding='utf8')
        print('Results dumped to all_results.csv')
        return df
    except Exception as e:
        print(filtered_res, 'excepted error ', e)
        df = pd.DataFrame(columns=['url', 'body', 'hashtags', 'comkeywords', 'diffkeywords', 'simscore', 'firstLine',
                                   'socialsharesReddit', 'socialsharesGoogle', 'socialsharesFb',
                                   'socialsharesLinkedin'])
        df['firstLine'] = ['No similar news articles obtained ']
        df = df.fillna(0)
        df.to_csv('media/all_results.csv', index=False, encoding='utf8')
        return df


if __name__ == '__main__':
    drive(sys.argv[1])

# changes made
# nltk deps installed in the file itself
# URL for each article added
# read from docx also
# added a filter threshold for sim that the user can play with
