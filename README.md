# Deploying the NLP App without docker
>On 2adpro-nextgen-spectra-dev-02 (IP: 10.10.30.212)

### Create nonRoot user:
```bash
ssh root@10.10.30.212
adduser nlpdev
usermod -aG sudo nlpdev
```


### Log out from server and login another time with
```bash
ssh nlpdev@10.10.30.212
sudo apt-get update
```


### Install anaconda:
```bash
cd /tmp
curl -O https://repo.anaconda.com/archive/Anaconda3-5.2.0-Linux-x86_64.sh
sha256sum Anaconda3-5.2.0-Linux-x86_64.sh
bash Anaconda3-5.2.0-Linux-x86_64.sh
```

### Add PATH to bashrc at the end:
```bash
sudo apt-get install nano
nano ~/.bashrc
```
Add at the end: 
```text
export PATH="/home/nlpdev/anaconda3/bin:$PATH" 
```


### Install git, ufw, clone project, install virtualenv
```bash
sudo apt-get update
sudo apt-get install git
sudo apt-get install ufw
git clone https://bitbucket.org/giorgiberiani/newsbot-application.git
cd newsbot-application
pip install virtualenv
virtualenv myprojectenv
source myprojectenv/bin/activate
pip install -r requirements.txt
python manage.py makemigrations
python manage.py migrate
python manage.py collectstatic
```


### Make Sure everything is okay so far:

```bash
gunicorn --bind 0.0.0.0:8000 myproject.wsgi
```

Go to the browser ip:8000 and site should be loaded, if that port is not open than it is okay if is not loading

### Configure gunicorn
```bash
sudo nano /etc/systemd/system/gunicorn.service
```
Add to the file:
```text
[Unit]
Description=gunicorn daemon
After=network.target

[Service]
User=nlpdev
Group=www-data
WorkingDirectory=/home/nlpdev/newsbot-application
ExecStart=/home/nlpdev/newsbot-application/newsbot-application/myprojectenv/bin/gunicorn --access-logfile - --workers 3 --bind unix:/home/nlpdev/newsbot-application/myproject.sock myproject.wsgi:application

[Install]
WantedBy=multi-user.target
```

### Start gunicorn:
```bash
sudo systemctl start gunicorn
sudo systemctl enable gunicorn
sudo systemctl status gunicorn
```
There should be no error in the last command

### Configure Nginx:
```bash
sudo apt-get update
sudo apt-get install nginx
sudo nano /etc/nginx/sites-available/myproject
```
Add to the file:
```text
server {
    listen 80;
    server_name 10.10.30.212;

    location = /favicon.ico { access_log off; log_not_found off; }
    location /static/ {
        root /home/nlpdev/newsbot-application;
    }

    location / {
        include proxy_params;
        proxy_pass http://unix:/home/nlpdev/newsbot-application/myproject.sock;
    }
}
```


```bash
sudo ln -s /etc/nginx/sites-available/myproject /etc/nginx/sites-enabled
sudo nginx -t
sudo systemctl restart nginx
sudo ufw delete allow 8000
sudo ufw allow 'Nginx Full'
```

You can now go to the http://10.10.30.212/myapp


# Using the Django Project, 
```text
Project contains one app called "myapp";
We use Sqlite3 database for the project(You can see it in the myproject/settings.py file.)

``` 

### Files included in the project app "myapp":
* admin.py
* driver.py
* forms.py
* main.py
* urls.py
* view.py

#### File admin.py 
```text
Usually registers admin panel for the project and here are settings for it. 
But currently we do not use any admin panel options. So it is as default
```

#### File driver.py
Contains functions: drive
##### Function drive
```text
After user uploads the file it is saved in "media" folder.
This function reads the file from this folder: Firstly checks if it is .txt, .docx or .csv and reads it accordingly. 
It does not work with .csv files currently so returns the value "Could not process your file. Please upload txt or docx file "
For "Good" input data it filters characters accordingly to the ordinal is smaller than 128.
Then called main.py file functions and gets the similarities. If no such thing found returns dataframe with  the above value in other case returns the similarity scores and article urls with the dataframe.

```
#### File forms.py
```text
Contains forms for the front end to get the document from user. There is only one field "docfile"
```
#### File models.py
```text
File contains models for the database; Currently we use two models: Doc and UserProfile;
Doc - This model has fields for writing doc resuls in the database, such as url, body, hashtags, simscore and etc.
UserProfile - This model is a child class of User model from Django, it has several fields like: username, password, email, is_active and etc.
```

#### File urls.py
```text
Contains endpoints for the site it like: /admin, /login, /logout, /home, /download; 
```

#### File views.py
```text
File contains endpoint back functions;
Function home() - renders main page; login is required
Function list() - Get's the file from user and saves it in the "media" folder. Calls the drive function from driver, this means gets the results from api.
                  renders the dashboard template with the results. 
                  If the method is not post it renders main page.
Function download() - Returns the file with the results named "all_results,csv"
```

#### File main.py
```text
Function get_wordnet_pos() - Maps the for the nltk pos tags
Function get_keywords_for_article() - Cleans the article and get the gensim keywords. If there is any keyword it 
                             returns keywords_from_tfidf. If not returns empty lists for the keywords.
Function get_news_aylien() -  Searches the term in api and returns searched dict.
Function get_facebook_shares() - Gets the facebook shares from api result.
Function get_sim_res() - Uses "get_keywords_for_article" to  get the keywords 
                        from article then sets the keywords and calls get_news_aylien to get 
                         the results and returns that results.
Function return_lsi_similarity_prob() - Results from api is compared with the input article and the similarity scores are kept in the list. Than this list is returned.
Function return_entities_keywords_percent() - Returns the percentage of keywords which are both in api and in input article over which are not in the input article and are in api.
Function get_final_similarity() - Counts the final similarity score as "lsi_s * sim_weight + ents_s * enty_weight". Returns the filtered list of similar articles sorted with respect to the final score.
```


#### API
```text
get auth token:
- curl -X POST http://HOST/rest/api-token-auth/ -d 'username=`USERNAME`&password=`PASSWORD`'

for access on API:
EXAMPLE:
    - curl -X POST http://HOST/rest/list/ -H 'Authorization: Token 4998d1043091a73a54bb138dfb3c185f71983609'


```