from django.contrib.auth.decorators import login_required
from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token

from rest.views import DocViewSet

app_name = 'rest'

urlpatterns = [
    path("list/", DocViewSet.as_view()),
    path('api-token-auth/', obtain_auth_token)
]
