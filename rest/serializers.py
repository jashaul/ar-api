from rest_framework.serializers import ModelSerializer

from myapp.models import Doc


class DocSerializer(ModelSerializer):

    class Meta:
        model = Doc
        fields = '__all__'
