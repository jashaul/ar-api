import tempfile

from django.contrib.auth import get_user_model
from django.test import TestCase, Client


# Create your tests here.
from rest_framework.test import APIClient


class ApiListView(TestCase):

    def setUp(self):
        self.user = get_user_model().objects.create(
            username="admin",
            email="admin@example.com",
            is_active=True,
            is_staff=True,
            is_superuser=True
        )
        self.user.set_password("admin123")
        self.user.save()
        self.client = Client()
        # self.client.force_login(user)

    def test_has_get_method(self):
        r = self.client.get('/rest/list/')
        self.assertIn(r.status_code, [405, 403, 401])

    def test_has_get_api_token_auth(self):
        r = self.client.post('/rest/api-token-auth/', data={
            "username": "admin",
            "password": "admin123"
        })
        self.assertEqual(r.status_code, 200)
        token = r.json().get("token")

        api_client = APIClient()

        with tempfile.NamedTemporaryFile(suffix='.txt', prefix='test') as f:
            r = api_client.post('/rest/list/', data={
                "file": f
            })
            self.assertEqual(r.status_code, 401)
            api_client.force_authenticate(user=self.user, token=token)
            r = api_client.post('/rest/list/', data={
                "file": f
            }, **{
                "Authorization": " Token %s" % token
            })
            self.assertEqual(r.status_code, 200)

