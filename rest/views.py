from django.core.files.storage import FileSystemStorage
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.status import HTTP_400_BAD_REQUEST
from rest_framework.views import APIView

from myapp.driver import drive
from myapp.models import Doc
from .serializers import DocSerializer
from rest_framework.response import Response


class DocViewSet(APIView):
    authentication_classes = (TokenAuthentication, )
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        if request.FILES.get("file") is None:
            return Response(status=HTTP_400_BAD_REQUEST)
        file = request.FILES['file']
        fs = FileSystemStorage()
        fs.save(file.name, file)
        uploaded_file_url = fs.path(file.name)
        filtered_res = drive(uploaded_file_url)
        entries = []
        for e in filtered_res.T.to_dict().values():
            entries.append(Doc(**e))
        Doc.objects.all().delete()
        Doc.objects.bulk_create(entries)
        queryset = Doc.objects.all()
        serializer = DocSerializer(queryset, many=True)
        return Response(serializer.data)
