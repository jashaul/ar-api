$(function (){
  /* Functions */
  var loadForm = function () {
    alert("1");
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',      
      beforeSend: function () {
        $("#modal-sim .modal-content").html("");
        $("#modal-sim").modal("show");
      },
      success: function (data) {
        $("#modal-sim .modal-content").html(data.html_form);
      }
    });
  };

 $("#sim-table #checkall").click(function () {
        if ($("#sim-table #checkall").is(':checked')) {
            $("#sim-table input[type=checkbox]").each(function () {
                $(this).prop("checked", true);
            });

        } else {
            $("#sim-table input[type=checkbox]").each(function () {
                $(this).prop("checked", false);
            });
        }
    });
    
    $("[data-toggle=tooltip]").tooltip();

  // var saveForm = function () {
  //   var form = $(this);
  //    alert("in save");
  //   $.ajax({
  //     url: form.attr("action"),
  //     data: form.serialize(),
  //     type: form.attr("method"),
  //     dataType: 'json',
  //     success: function (data) {
  //       if (data.form_is_valid) {
  //         $("#sim-table").html(data.html_control_list);
  //         $("#modal-sim").modal("hide");
  //       }
  //       else {
  //         $("#modal-sim .modal-content").html(data.html_form);
  //       }
  //     }
  //   });
  //   return false;
  // };

  /* Binding */

  //select simulation
  //  $(".js-select-sim").click(loadForm);

$("#sim-table").on("click", ".js-select-sim", loadForm);
// $("#modal-sim").on("submit", ".js-select-sim-form", saveForm);

});
