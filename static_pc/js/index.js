$(document).ready(function() {

    $('.tabs').tabs({});
    $('.statusselect').find('.select-wrapper').addClass('greyGradient');
    $('.filesselect,.subtaskselect').find('.select-wrapper').addClass('blueGradient');

    $('.circle_expand i').click(function(){
    	$(this).parent().siblings('.changeDetails').find('.changeDetailsDesc').toggleClass('expand');
    	if($(this).html() == 'remove_circle_outline'){
    		$(this).html('add_circle_outline'); 
    	}else{
    		$(this).html('remove_circle_outline'); 
    	}
    });

    $('.add_circle i').click(function(){
    	$(this).parent().siblings().find('.usermessage').toggleClass('expand');
    	if($(this).html() == 'remove_circle_outline'){
    		$(this).html('add_circle_outline'); 
    	}else{
    		$(this).html('remove_circle_outline'); 
    	}
    });

    $('.scroll-animate ul li a').click(function(){
    	var target = $(this).attr('target');
    	var parent = $(this).parents('.scroll');
        $(parent).find('.content-header ul li a').removeClass('active');
        $(this).addClass('active');
    	$('html, body').animate({
    	    scrollTop: ($(parent).find('.'+target).offset().top-168)
    	}, 500);
        $('body').getNiceScroll().resize();
    });


    $('.scroll-animatepopup ul li a').click(function(){
      var self = $(this);
      console.log(self);
      var target = $(this).attr('target');
      console.log($(target));
      $('#subtaskcreate').animate({
        scrollTop: 0
      }, 0);
      console.log($('.'+target).offset().top);
      var parent = $(this).parents('.scroll');
        $(parent).find('.content-header ul li a').removeClass('active');
        $(this).addClass('active');
        $('#subtaskcreate').animate({
          scrollTop: ($('#subtaskcreate').find('.'+target).offset().top - 836 )
      }, 300);
      $('#subtaskcreate').getNiceScroll().resize();
    })









    $('#search, #createsearch').on('focus', function(){
      $(this).parent().css({'border':'1px solid #399dff','background':'#fff'});
    });

    $('#search, #createsearch').on('blur', function(){
      $(this).parent().css({'border':'1px solid rgba(0, 0, 0, 0.08)','background':'#f5f5f5'});
    });

    $('.search .close').click(function(){
      $('#search').val('');
      $('.search .close').css('opacity','0');
      $('.search .select-wrapper').css('opacity','0');
    })


    function DropDown(el) {
        this.dd = el;
        this.initEvents();
    }

    DropDown.prototype = {
        initEvents : function() {
            var obj = this;

            obj.dd.on('click', function(event){
              if(event.target.type != "file"){
              if(!$(event.target).parents('.dropdown-contents').length && $(event.target).parents('.dropdown').find('.dropdown-contents').hasClass('active')){
                $('.dropdown-contents').removeClass('active');
                $('[data-position]').removeClass('tt');
                $('select[data-nametag]').parent().removeClass('active');
                event.stopPropagation();
              }
              else if($(event.target).parents('.dropdown-contents').length && !$(event.target).closest('.dropdown-contents').hasClass('stay')){
                $('.dropdown-contents').removeClass('active');
                $('[data-position]').removeClass('tt');
                $('select[data-nametag]').parent().removeClass('active');
                //$(this).find('.dropdown-contents').addClass('active');
                // console.log('2');
                if($(event.target).parents('#savedSearchesBtn').length || $(event.target).hasClass('#savedSearchesBtn')){
                  var elem = $('#savedSearch');
                  var instance = M.Modal.getInstance(elem);
                  instance.open();
                }
                else{
                  event.stopPropagation();
                }
                }
                else{
                  // console.log('4');
                  $('.dropdown-contents').removeClass('active');
                  $('[data-position]').removeClass('tt');
                  $('select[data-nametag]').parent().removeClass('active');
                  $(this).find('.dropdown-contents').addClass('active');
                  $(this).find('[data-position]').addClass('tt');
                  event.stopPropagation();
                }
              }});
        }
    }

    var dd = new DropDown($('.dropdown.plainPop'));

    $(document).click(function(event) {
      if(!$(event.target).closest('.dropdown-contents').hasClass('stay')){
        $('.dropdown.plainPop').find('.dropdown-contents').removeClass('active');
        $('[data-position]').removeClass('tt');
        $('select[data-nametag]').parent().removeClass('active');
      }
      else{
      }
    });

    $('.menu-link').click(function(){
      $('#sideNav').toggleClass('active');
      $('body').toggleClass('sideMenuOpen');
      $(this).toggleClass('change');
    });

    $('#sideNavTab').tabs({});

    // $('.order-by').find('li a').click(function(){
    //   var selectedText = $(this).attr('name');
    //   that = $(this);
    //   $(this).parent().parent().parent().parent().find('a.hover-link span').text(selectedText);
    //   setTimeout(function(){that.closest('.dropdown-contents').removeClass('active');}, 100);
    // });

    // $('.checkbox.intial').click(function(){
    //     if($(this).attr('data-target') && !$(this).hasClass('active')){
    //         $(this).parents('table,.table').find('.checkbox.intial').addClass('active');
    //         $(this).parents('table,.table').find('input[type="checkbox"]').attr('checked',true);
    //     }else if($(this).attr('data-target') && $(this).hasClass('active')){
    //         $(this).parents('table,.table').find('.checkbox.intial').removeClass('active');
    //         $(this).parents('table,.table').find('input[type="checkbox"]').attr('checked',false);
    //     }else if(!$(this).attr('data-target') && $(this).hasClass('active')){
    //         $(this).removeClass('active');
    //         $(this).siblings('input[type="checkbox"]').attr('checked',false)
    //     }else if(!$(this).attr('data-target') && !$(this).hasClass('active')){
    //         $(this).addClass('active');
    //         $(this).siblings('input[type="checkbox"]').attr('checked',true)
    //     }
    // });

    $(document).delegate('.workflow', 'click', function(){
        $('.bodypopup').fadeIn(300);
        setTimeout(function(){window.dispatchEvent(new Event('resize'));}, 200);
        $('#jobdetails, #pagepoup, #createJobCon, #subtaskcreate').addClass('preventScroll');

    });

    $(document).on('click', '.linkedHover', function(){
        $('.linkItems').fadeIn(300);
        setTimeout(function(){window.dispatchEvent(new Event('resize'));}, 200);
        $('#jobdetails, #pagepoup, #createJobCon, #subtaskcreate').addClass('preventScroll');

    });
    
    $('.popup .editOperations.close,.modal-content .editOperations.close').click(function(){
        $('.bodypopup').fadeOut(500);
        $('.statuspopup').fadeOut(500);
        $('.modal.fullscreen').fadeOut(500);
        $('.createmenupopup').fadeOut(500);
        $('.savedFilter').fadeOut(500);
        $('.saveFilter').fadeOut(500);
        $('#jobdetails, #pagepoup, #createJobCon, #subtaskcreate').removeClass('preventScroll');
        setTimeout(function(){window.dispatchEvent(new Event('resize'));}, 200)
    });

    $('.createmenu').click(function(){
        $('.createmenupopup').fadeIn(500);
    });


    $(document).on('click', '#createTaskWrap',function(){
      // $('a#dashboard')[0].click();
      $('.createmenupopup').fadeIn(300);
      setTimeout(function(){window.dispatchEvent(new Event('resize'));}, 200);
    })

    $('.statusdropdown li a').click(function(){
      var self = $(this);
        $('.statuspopup').fadeIn(500);
        $('.status-body span').text(self.attr('name'));
        $('#jobdetails, #pagepoup, #createJobCon, #subtaskcreate').addClass('preventScroll');
    });

    $('.content-header .editOperations.edit').click(function(){
        $('.detailstask-edit').fadeIn(500);
        var parent = $(this).parents('.scroll');
        $(this).parents('.content-header').attr('edit-target',parent.attr('id'));
        $(this).parents('.content-header').find('span.header').attr('contenteditable',true);
        $(parent).find('.details-content input').removeAttr('readonly');
        $(parent).find('.task input').not('[data-tracking-no]').removeAttr('readonly');
        $(parent).find('.task textarea').removeAttr('readonly');
        $(parent).find('.deadline-datepicker,.created-datepicker,.updated-datepicker').removeClass('datepicker');
        datepickerUpdate();
        datepickerDead();
        datepickerCreate();
        $('.sidebar-mini').css('padding-bottom','45px');

        // var elems = document.querySelector('#userlist_task');
        // var instances = M.Dropdown.init(elems, {constrainWidth:false, coverTrigger:false, alignment:'bottom', closeOnClick:false});

        $('.assigneeListTrigger').css({'pointer-events':'all'});

        // var elemsSSS = document.querySelector('#subuserlist_task');
        // var instancesSSS = M.Dropdown.init(elemsSSS, {constrainWidth:false});

        var elemS = document.querySelectorAll('.contentBox select');
        Array.from(elemS).forEach(function(val, index){          
          var slInstance = M.FormSelect.getInstance(val);
          slInstance.destroy();
        })

        $('.contentBox select').attr('disabled', false);

        var elemSS = document.querySelectorAll('.contentBox select');
        Array.from(elemSS).forEach(function(val, index){ 
          var instances = M.FormSelect.init(val, {dropdownOptions: {coverTrigger: false}});
        })

        $('#jobdetails').removeClass('view').addClass('edit');

    });

    $('.btn-discard').click(function(){
        var parent = $('body').find('div[edit-target]').parents('.scroll');
        $(parent).find('.details-content input').attr('readonly',true);
        $(parent).find('.content-header span.header').attr('contenteditable',false);
        $(parent).find('.task input').attr('readonly',true);
        $(parent).find('.task textarea').attr('readonly',true);
        $(parent).find('.deadline-datepicker,.created-datepicker,.updated-datepicker').addClass('datepicker');
        $('.detailstask-edit').fadeOut(500);
        $('.sidebar-mini').css('padding-bottom','0');
        $('body').removeAttr('div[edit-target]');

        $('.assigneeListTrigger').css({'pointer-events':'none'});

        var elemS = document.querySelectorAll('.contentBox select');
        Array.from(elemS).forEach(function(val, index){          
          var slInstance = M.FormSelect.getInstance(val);
          slInstance.destroy();
        })

        $('.contentBox select').attr('disabled', true);

        var elemSS = document.querySelectorAll('.contentBox select');
        Array.from(elemSS).forEach(function(val, index){ 
          var instances = M.FormSelect.init(val, {dropdownOptions: {coverTrigger: false}});
        })

        $('#jobdetails').removeClass('edit').addClass('view');

    });

    $('.btn-save').click(function(){
      $('.statuspopup').fadeIn(200);
    })

    $('.btn-done').click(function(){
        var parent = $('body').find('div[edit-target]').parents('.scroll');
        $('.statuspopup').hide();
        $('#subtaskcreate').hide();
        $('#saveFilter').hide();
        $('#savedFilter').hide();
        $(parent).find('.details-content input').attr('readonly',true);
        $(parent).find('.content-header span.header').attr('contenteditable',false);
        $(parent).find('.task input').attr('readonly',true);
        $(parent).find('.task textarea').attr('readonly',true);
        $(parent).find('.deadline-datepicker,.created-datepicker,.updated-datepicker').addClass('datepicker');
        $('.detailstask-edit').fadeOut(500);
        $('.sidebar-mini').css('padding-bottom','0');
        $('body').removeAttr('div[edit-target]');

        $('.assigneeListTrigger').css({'pointer-events':'none'});

        var elemS = document.querySelectorAll('.contentBox select');
        Array.from(elemS).forEach(function(val, index){          
          var slInstance = M.FormSelect.getInstance(val);
          slInstance.destroy();
        })

        $('.contentBox select').attr('disabled', true);

        var elemSS = document.querySelectorAll('.contentBox select');
        Array.from(elemSS).forEach(function(val, index){ 
          var instances = M.FormSelect.init(val, {dropdownOptions: {coverTrigger: false}});
        })

        $('#jobdetails').removeClass('edit').addClass('view');


    });

    $('.border-bot-table.job').click(function(){
        if($(this).hasClass('add')){
          $(this).parent().find($('.jobdetailspopup')).remove();
          $(this).parent().children().removeClass('remove');
          $(this).parent().children().addClass('add');
          $(this).after($('.jobdetailsparentdiv').html()).show('slow');
          var ddcreate = new DropDown($('.dropdown.create'));
          $(this).addClass('remove');
          $(this).removeClass('add');
        }else if($(this).hasClass('remove')){
          $(this).parent().find($('.jobdetailspopup')).remove();
          $(this).removeClass('remove');
          $(this).addClass('add');
        }
        $('#createjob .activejobs').getNiceScroll().resize();
    });


    $('.save-filters').click(function(){
      $('.saveFilter').fadeIn(300);
    })

    $('.saved-filters').click(function(){
      $('.savedFilter').fadeIn(300);
    })


    $('.create').click(function(){
      $('.detailstask-edit').fadeOut(500); 
      $('body').css('padding','0');
      $('.drafts').css('display','block');
      $('.border-bot-table.header').css('display','block');
    });

    $('#dashboard').click(function(){
      var parent = $('body').find('div[edit-target]').parents('.scroll');
      $(parent).find('.details-content input').attr('readonly',true);
      $(parent).find('.content-header span.header').attr('contenteditable',false);
      $(parent).find('.task input').attr('readonly',true);
      $(parent).find('.task textarea').attr('readonly',true);
      $('body').getNiceScroll().resize();
    });
      
    $('body').niceScroll({
      cursorcolor:"rgba(0,0,0,1)",
      cursoropacitymax:1,
      cursorwidth:3,
      cursorborder:"0px solid rgba(0,0,0,0.8)",
      cursorborderradius:"8px",
      background:"#DDD",
      autohidemode: true,
      mousescrollstep: '20',
      horizrailenabled:false
    });

    $('.loadmore a').click(function(){
        var div = $('.usersmessages  .row').clone();
        $('.usersmessages').append(div);
    });

    $('.popup-tabs').niceScroll({
      cursorcolor:"rgba(0,0,0,1)",
      cursoropacitymax:1,
      cursorwidth:3,
      cursorborder:"0px solid rgba(0,0,0,0.8)",
      cursorborderradius:"8px",
      background:"#DDD",
      autohidemode: true,
      mousescrollstep: '20',
      horizrailenabled:false
    });

    $('.modal.fullscreen').niceScroll({
      cursorcolor:"rgba(0,0,0,1)",
      cursoropacitymax:1,
      cursorwidth:3,
      cursorborder:"0px solid rgba(0,0,0,0.8)",
      cursorborderradius:"8px",
      background:"#DDD",
      autohidemode: true ,
      mousescrollstep: '20',
      horizrailenabled:false
    });

    $('#createjob .activejobs').niceScroll({
      cursorcolor:"rgba(0,0,0,1)",
      cursoropacitymax:1,
      cursorwidth:3,
      cursorborder:"0px solid rgba(0,0,0,0.8)",
      cursorborderradius:"8px",
      background:"#DDD",
      autohidemode: true,
      mousescrollstep: '20',
      horizrailenabled:false
    });

    $('#tasklistScroll').niceScroll({
      cursorcolor:"rgba(0,0,0,1)",
      cursoropacitymax:1,
      cursorwidth:3,
      cursorborder:"0px solid rgba(0,0,0,0.8)",
      cursorborderradius:"8px",
      background:"#DDD",
      autohidemode: true,
      mousescrollstep: '20',
      horizrailenabled:false
    });

    $('.createorders').niceScroll({
      cursorcolor:"rgba(0,0,0,1)",
      cursoropacitymax:1,
      cursorwidth:3,
      cursorborder:"0px solid rgba(0,0,0,0.8)",
      cursorborderradius:"8px",
      background:"#DDD",
      autohidemode: true,
      mousescrollstep: '20',
      horizrailenabled:false,
      scrollbarid: 'createOrder'
    });

    $('.popup-tabs.table').niceScroll({
      cursorcolor:"rgba(0,0,0,1)",
      cursoropacitymax:1,
      cursorwidth:3,
      cursorborder:"0px solid rgba(0,0,0,0.8)",
      cursorborderradius:"8px",
      background:"#DDD",
      autohidemode: true,
      mousescrollstep: '20',
      horizrailenabled:false,
      scrollbarid: 'workflow'
    });

    $('.popup-tabs .selectedLinked >div , .popup-tabs .selectedToLinked > div').niceScroll({
      cursorcolor:"rgba(0,0,0,1)",
      cursoropacitymax:1,
      cursorwidth:3,
      cursorborder:"0px solid rgba(0,0,0,0.8)",
      cursorborderradius:"8px",
      background:"#DDD",
      autohidemode: true,
      mousescrollstep: '20',
      horizrailenabled:false,
      scrollbarid: 'workflow'
    });

    $('.reportTableWrap').niceScroll({
      cursorcolor:"rgba(0,0,0,1)",
      cursoropacitymax:1,
      cursorwidth:3,
      cursorborder:"0px solid rgba(0,0,0,0.8)",
      cursorborderradius:"8px",
      background:"#DDD",
      autohidemode: true,
      mousescrollstep: '20',
      horizrailenabled:false,
      scrollbarid: 'reportsScroll'
    });
    $('.mailmenutooltip + .dropdown-contents .dpMenuWrap, .navsmenutooltip + .dropdown-contents .dpMenuWrap').niceScroll({
      cursorcolor:"rgba(0,0,0,1)",
      cursoropacitymax:1,
      cursorwidth:3,
      cursorborder:"0px solid rgba(0,0,0,0.8)",
      cursorborderradius:"8px",
      background:"#DDD",
      autohidemode: true,
      mousescrollstep: '20',
      horizrailenabled:false,
      scrollbarid: 'mailmenutooltipScroll'
    });

    $('.searchDropdown').niceScroll({
      cursorcolor:"rgba(0,0,0,1)",
      cursoropacitymax:1,
      cursorwidth:3,
      cursorborder:"0px solid rgba(0,0,0,0.8)",
      cursorborderradius:"8px",
      background:"#DDD",
      autohidemode: true,
      mousescrollstep: '20',
      horizrailenabled:false,
      scrollbarid: 'searchDropdownConScroll'
    });

    $(document).on('click', 'th .commonCheck input', function(){
      if($(this).is(':checked')){
        $(this).parents('table').find('tbody td:first-child input').prop('checked', true);
        $(this).parents('.contentBox').find('.headingtagWrap .editOperations.delete').show();
      }
      else{
        $(this).parents('table').find('tbody td:first-child input').prop('checked', false);
        $(this).parents('.contentBox').find('.headingtagWrap .editOperations.delete').hide();
      }
    })



    $(document).on('click', '.contentBox td:first-child [type="checkbox"]', function(){
      if($(this).is(':checked')){
        $(this).parents('.contentBox').find('.headingtagWrap .editOperations.delete').show();
      }
      else{
        $(this).parents('.contentBox').find('.headingtagWrap .editOperations.delete').hide();
      }
    })

    // CKEDITOR.replace( 'editor1' );
    // CKEDITOR.replace( 'editor1',
    // {
    //     toolbar : 'Basic', /* this does the magic */
    //     uiColor : '#9AB8F3'
    // });
    // CKEDITOR.config.toolbar = [
    //    ['Styles','Format','Font','FontSize'],
    //    '/',
    //    ['Bold','Italic','Underline','StrikeThrough','-','Undo','Redo','-','Cut','Copy','Paste','Find','Replace','-','Outdent','Indent','-','Print'],
    //    '/',
    //    ['NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
    //    ['Image','Table','-','Link','Flash','Smiley','TextColor','BGColor','Source']
    // ] ;

    $('a[name="Versions"]').click(function(){
        $('#pagepoup').fadeIn(500);
        // $('.createBottombar').show(300);
    })

    $(window).scroll(function(){
        $('body').getNiceScroll().resize();
        $('.popup-tabs').getNiceScroll().resize(); 
        $('#createjob .activejobs').getNiceScroll().resize();
    })



    var start2 = moment().subtract(29, 'days');

    function drpFormat(start3){
      setTimeout(function(){$(".deadline-datepicker").val(start3.format('DD/MMM/YYYY'));}, 10);
    }

    function datepickerDead(){
      $('.deadline-datepicker').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        autoApply: true,
        startDate: start2,
        locale: {
        format: 'DD/MMM/YYYY'
      }
    }, drpFormat(start2));
    }
  
    $(".deadline-datepicker").on('hide.daterangepicker', function(ev, picker){
      drpFormat(picker.startDate, picker.endDate);
    });

    drpFormat(start2);

    

    function drpFormat1(start3){
      setTimeout(function(){$(".created-datepicker").val(start3.format('DD/MMM/YYYY'));}, 10);
    }

    var start2 = moment().subtract(29, 'days');
    
    function datepickerCreate(){
      $('.created-datepicker').daterangepicker({
          singleDatePicker: true,
          showDropdowns: true,
          autoApply: true,
          startDate: start2,
          locale: {
          format: 'DD/MMM/YYYY'
        }
    // maxYear: end2.year()
    }, drpFormat1(start2));
    }
    

    $(".created-datepicker").on('hide.daterangepicker', function(ev, picker){
      drpFormat1(picker.startDate, picker.endDate);
    });

    drpFormat1(start2);




    function drpFormat2(start3){
      setTimeout(function(){$(".updated-datepicker").val(start3.format('DD/MMM/YYYY'));}, 10);
    }

    var start2 = moment().subtract(29, 'days');
    
    function datepickerUpdate(){
      $('.updated-datepicker').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        autoApply: true,
        startDate: start2,
        locale: {
        format: 'DD/MMM/YYYY'
      }
    }, drpFormat2(start2));
    }
    
    $(".updated-datepicker").on('hide.daterangepicker', function(ev, picker){
      drpFormat1(picker.startDate, picker.endDate);
    });

    drpFormat2(start2);


    var elems = document.querySelectorAll('select');
    Array.from(elems).forEach(function(val, index){  
      var instances = M.FormSelect.init(val);
    })





$('#landingScreen, .btn-discardEdt').click(function(e){
  $('#subtaskcreate').hide(); //subtask create
  $('.body-overlay').hide(); //body overlay for popup
  // $('#pagepoup').hide()   //subtask view/edit
  // $('#task').show();   //landing page
  // $('#createTaskWrap').hide();   //plus icon
  // $('#jobdetails').hide();   //task view/edit
  // $('#createJobCon').hide();   //create task
  $('.detailstask-edit').hide();   //discard changes
  $('#pagepoupPop').hide();   //landing page
  $('.createBottombar').hide();   //create another
  

  $('html, body').animate({
      scrollTop: ($('.scroll').find('.detailsSS').offset().top)
  }, 0);

  $('html, body').animate({
    scrollTop: ($('.scroll').find('.details').offset().top-168)
  }, 0);

  // var elem = document.querySelector('#userlist_task');
  // var udInstance = M.Dropdown.getInstance(elem);
  // if(udInstance) udInstance.destroy();

  $('.assigneeListTrigger').css({'pointer-events':'none'});

  // var elemSSS = document.querySelector('#subuserlist_task');
  // var udInstanceSSS = M.Dropdown.getInstance(elemSSS);
  // if(udInstanceSSS) udInstanceSSS.destroy();


  var elemS = document.querySelectorAll('.contentBox select');
  Array.from(elemS).forEach(function(val, index){          
    var slInstance = M.FormSelect.getInstance(val);
    slInstance.destroy();
  })

  $('.contentBox select').attr('disabled', true);

  var elemSS = document.querySelectorAll('.contentBox select');
  Array.from(elemSS).forEach(function(val, index){  
  var instances = M.FormSelect.init(val, {dropdownOptions: {coverTrigger: false}});
})
})

$('#dashboard').click(function(e){
      $('#subtaskcreate').hide(); //subtask create
      $('.body-overlay').hide(); //body overlay for popup
      // $('#pagepoup').hide()   //subtask view/edit
      // $('#task').hide();   //landing page
      // $('#createTaskWrap').hide();   //plus icon
      // $('#jobdetails').show();   //task view/edit
      // $('#createJobCon').hide();   //create task
      $('.detailstask-edit').hide();   //discard changes
      $('#pagepoupPop').hide();   //landing page
      $('.createBottombar').hide();   //create another
    
      $('html, body').animate({
          scrollTop: ($('.scroll').find('.detailsSS').offset().top)
      }, 0);
    
      $('html, body').animate({
        scrollTop: ($('.scroll').find('.details').offset().top-168)
      }, 0);

      // var elem = document.querySelector('#userlist_task');
      // var udInstance = M.Dropdown.getInstance(elem);
      // udInstance.destroy();

      // var elemSSS = document.querySelector('#subuserlist_task');
      // var udInstanceSSS = M.Dropdown.getInstance(elemSSS);
      // udInstanceSSS.destroy();
      $('.assigneeListTrigger').css({'pointer-events':'none'});


      var elemS = document.querySelectorAll('.contentBox select');
      Array.from(elemS).forEach(function(val, index){          
        var slInstance = M.FormSelect.getInstance(val);
        slInstance.destroy();
      })

      $('.contentBox select').attr('disabled', true);

      var elemSS = document.querySelectorAll('.contentBox select');
      Array.from(elemSS).forEach(function(val, index){  
      var instances = M.FormSelect.init(val, {dropdownOptions: {coverTrigger: false}});
    })
  })

$('.subtaskid').click(function(e){
    $('#subtaskcreate').hide(); //subtask create
    $('.body-overlay').hide(); //body overlay for popup
    // $('#pagepoup').show();   //subtask view/edit
    // $('#task').hide();   //landing page
    // $('#createTaskWrap').hide();   //plus icon
    // $('#jobdetails').hide();   //task view/edit
    // $('#createJobCon').hide();   //create task
    $('.detailstask-edit').hide();   //discard changes
    $('#pagepoupPop').hide();   //landing page
    $('.createBottombar').hide();   //create another
  
    $('html, body').animate({
        scrollTop: ($('.scroll').find('.detailsSS').offset().top)
    }, 0);
  
    $('html, body').animate({
      scrollTop: ($('.scroll').find('.details').offset().top-168)
    }, 0);


  $('.deadline-datepicker,.created-datepicker,.updated-datepicker').addClass('datepicker');

  // var elem = document.querySelector('#userlist_task');
  // var udInstance = M.Dropdown.getInstance(elem);
  // udInstance.destroy();

  // var elemSSS = document.querySelector('#subuserlist_task');
  // var udInstanceSSS = M.Dropdown.getInstance(elemSSS);
  // udInstanceSSS.destroy();
  $('.assigneeListTrigger').css({'pointer-events':'none'});


  var elemS = document.querySelectorAll('.contentBox select');
  Array.from(elemS).forEach(function(val, index){          
    var slInstance = M.FormSelect.getInstance(val);
    slInstance.destroy();
  })

  $('.contentBox select').attr('disabled', true);

  var elemSS = document.querySelectorAll('.contentBox select');
  Array.from(elemSS).forEach(function(val, index){  
  var instances = M.FormSelect.init(val, {dropdownOptions: {coverTrigger: false}});
    
  
  })

})

$('.subTasks li.versionTypeLi').click(function(e){
    $('#subtaskcreate').fadeIn(500); //subtask create
    $('.body-overlay').show(); //body overlay for popup
    $('.createBottombarPop').show();   //create another
    // $('#pagepoup').hide();   //subtask view/edit
    // $('#task').hide();   //landing page
    // $('#createTaskWrap').hide();   //plus icon
    // $('#jobdetails').hide();   //task view/edit
    // $('#createJobCon').hide();   //create task
    // $('.detailstask-edit').hide();   //discard changes
    // $('#pagepoupPop').hide();   //landing page
  
    // $('html, body').animate({
    //     scrollTop: ($('.scroll').find('.detailsSS').offset().top)
    // }, 0);
  
    // $('html, body').animate({
    //   scrollTop: ($('.scroll').find('.details').offset().top-168)
    // }, 0);

    datepickerUpdate();
    datepickerDead();
    datepickerCreate();

  // $('.deadline-datepicker,.created-datepicker,.updated-datepicker').addClass('datepicker');

  // var elem = document.querySelector('#userlist_task');
  // var udInstance = M.Dropdown.getInstance(elem);
  // if(udInstance) udInstance.destroy();

  // var elemSSS = document.querySelector('#subuserlist_task');
  // var udInstanceSSS = M.Dropdown.getInstance(elemSSS);
  // if(udInstanceSSS) udInstanceSSS.destroy();
  $('.assigneeListTrigger').css({'pointer-events':'all'});

  // 

  var elemS = document.querySelectorAll('.contentBox select');
  Array.from(elemS).forEach(function(val, index){          
    var slInstance = M.FormSelect.getInstance(val);
    slInstance.destroy();
  })

  $('.contentBox select').attr('disabled', false);

  var elemSS = document.querySelectorAll('.contentBox select');
  Array.from(elemSS).forEach(function(val, index){  
  var instances = M.FormSelect.init(val, {dropdownOptions: {coverTrigger: false}});
    
  
  })

  // var elemsSSSS = document.querySelector('#subuserlist_taskcreate');
  // var instancesSSSS = M.Dropdown.getInstance(elemSSS);
  // if(!instancesSSSS) var instancesSSSS = M.Dropdown.init(elemsSSSS, {constrainWidth:false});

  

  setTimeout(function(){
    $('#subtaskcreate .created-datepicker, #subtaskcreate .deadline-datepicker, #subtaskcreate .updated-datepicker').removeClass('datepicker').val('')
  }, 200);

})

$('.editOperations.close').click(function(){
  $('.body-overlay').hide();
  $('.createBottombar').hide();
  $('.createBottombarPop').hide();   //create another
  $('.deletePopup').hide();
  $('.linkItems').hide();
  $('.searchDropdown').hide();
})


$('#search').on('keyup', function(){
  if($(this).val().trim() != ""){
    $('.searchDropdown').fadeIn(300);
    $('.search .close').css('opacity','1');
    $('.search .select-wrapper').css('opacity','1');
  }
  else{
    $('.searchDropdown').fadeOut(200);
  }
})


$('.createorders .btn').on('click', function(){

    var instance = M.Tabs.getInstance($('#sideNavTab'));
    instance.select('createJobCon');

    $('.createmenupopup').hide();

    $('#subtaskcreate').hide(); //subtask create
      $('.body-overlay').hide(); //body overlay for popup
      // $('#pagepoup').hide()   //subtask view/edit
      // $('#task').hide();   //landing page
      // $('#createTaskWrap').hide();   //plus icon
      // $('#jobdetails').hide();   //task view/edit
      // $('#createJobCon').show();   //create task
      $('.detailstask-edit').hide();   //discard changes
      $('#pagepoupPop').hide();   //landing page
      $('.createBottombar').not('.createBottombarPop').show();   //create another
      datepickerUpdate();
      datepickerDead();
      datepickerCreate();
    
      $('html, body').animate({
          scrollTop: ($('.scroll').find('.detailsSS').offset().top)
      }, 0);
    
      $('html, body').animate({
        scrollTop: ($('.scroll').find('.details').offset().top-168)
      }, 0);

      // var elemsSSSS = document.querySelector('#createuserlist_task');
      // M.Dropdown.init(elemsSSSS, {constrainWidth:false});
      $('.assigneeListTrigger').css({'pointer-events':'all'});

      var elemS = document.querySelectorAll('.contentBox select');
      Array.from(elemS).forEach(function(val, index){          
        var slInstance = M.FormSelect.getInstance(val);
        slInstance.destroy();
      })

      $('.contentBox select').attr('disabled', false);

      var elemSS = document.querySelectorAll('.contentBox select');
      Array.from(elemSS).forEach(function(val, index){  
      var instances = M.FormSelect.init(val, {dropdownOptions: {coverTrigger: false}});
    })
    setTimeout(function(){
      $('#createJobCon .created-datepicker, #createJobCon .deadline-datepicker, #createJobCon .updated-datepicker').val('')
    }, 200);
})


$('.editOperations.delete').click(function(){
  $('.deletePopup').show();
})


$('.toTask').click(function(){
  var instance = M.Tabs.getInstance($('#sideNavTab'));
  instance.select('jobdetails');
  
})


$('.btn-delete').click(function(){
  $('.deletePopup').hide();
  var instance = M.Tabs.getInstance($('#sideNavTab'));
  instance.select('jobdetails');
})



 // pagelload
 // $('#createTaskWrap').show();   //plus icon
  // $('#jobdetails').hide();   //task view/edit
  // $('#pagepoup').hide()   //subtask view/edit
  // $('#createJobCon').hide();   //create task
  
  // $('#task').show();   //landing page
  // $('.detailstask-edit').hide();   //discard changes
  // $('#pagepoupPop').hide();   //landing page
  // $('.createBottombar').hide();   //create another

  $('html, body').animate({
      scrollTop: ($('.scroll').find('.detailsSS').offset().top)
  }, 0);

  $('html, body').animate({
    scrollTop: ($('.scroll').find('.details').offset().top-168)
  }, 0);


  $('.deadline-datepicker,.created-datepicker,.updated-datepicker').addClass('datepicker');
  

  // var elems = document.querySelector('#userlist_task');
  // var instances = M.Dropdown.init(elems, {constrainWidth:false});
  // var elem = document.querySelector('#userlist_task');
  // var udInstance = M.Dropdown.getInstance(elem);
  // if(udInstance) udInstance.destroy();

  // var elemSSSSS = document.querySelector('#subuserlist_task');
  // var udInstanceS = M.Dropdown.getInstance(elemSSSSS);
  // if(udInstanceS) udInstanceS.destroy();

  // var elemsSSS = document.querySelector('#createuserlist_task');
  // var instancesSSS = M.Dropdown.init(elemsSSS, {constrainWidth:false});

  // var elemsSSSS = document.querySelector('#subuserlist_taskcreate');
  // var instancesSSSS = M.Dropdown.init(elemsSSSS, {constrainWidth:false});
  


  var elemS = document.querySelectorAll('.contentBox select');
  Array.from(elemS).forEach(function(val, index){          
    var slInstance = M.FormSelect.getInstance(val);
    slInstance.destroy();
  })

  $('.contentBox select').attr('disabled', true);

  var elemSS = document.querySelectorAll('.contentBox select');
  Array.from(elemSS).forEach(function(val, index){  
    var instances = M.FormSelect.init(val);
  })


$(document).on('click', '#tasklist .wrapTable:not(.rotate)', function(){
  $('table .wrap').hide();
  // $(this).toggleClass('active');
  $(this).next().find('.wrap').slideToggle(300);
  $('#tasklist .wrapTable').removeClass('rotate');
  $(this).toggleClass('rotate');
  setTimeout(function(){window.dispatchEvent(new Event('resize'));}, 200);
})
$(document).on('click', '#tasklist .wrapTable.rotate', function(){
  $(this).next().find('.wrap').slideUp(300);
  $(this).removeClass('rotate');
  setTimeout(function(){window.dispatchEvent(new Event('resize'));}, 200);
})

$(document).on('click', function(){
  $('.searchDropdown').hide();
})


$('.dropdown.plainPop.contentFilter li').click(function(){
  var nameActual = $(this).parents('.dropdown.plainPop.contentFilter').children('a').attr('name');
  var titleActual = $(this).parents('.dropdown.plainPop.contentFilter').children('a').attr('title');

  var badgeText = $(this).find('.badge').text();
  var title = $(this).children().not('span').text();
  var badgeText = $(this).find('.badge').text();
  var tableID = $(this).find('a').attr('name');
  $(this).parent().find('li').removeClass('active');
  $(this).addClass('active');
  $(this).parents('.dropdown.plainPop.contentFilter').children('a').html(title+'<i class="material-icons">arrow_drop_down</i>');
  $(this).parents('.dropdown.plainPop.contentFilter').children('a').attr('name', tableID);
  $(this).parents('.dropdown.plainPop.contentFilter').children('a').attr('title', title);
  $('#task').find('.badgeCircle').text(badgeText);
  $('#task .headingtag').text(title).attr('data-taskType',title);
  $('#task').find('.table.mainTable').hide();
  $('#task').find('#' + tableID).show();

  $(this).parents('.dropdown-contents').find('a').text(titleActual);
  $(this).parents('.dropdown-contents').find('a').attr('name',nameActual);


  setTimeout(function(){window.dispatchEvent(new Event('resize'));}, 200);
})

  var def = $('.dropdown.plainPop.contentFilter li.active')
  var titleD = def.children().not('span').text();
  var badgeTextD = def.find('.badge').text();
  var tableIDD = def.find('a').attr('name');

  var nameActualD = def.parents('.dropdown.plainPop.contentFilter').children('a').attr('name');
  var titleActualD = def.parents('.dropdown.plainPop.contentFilter').children('a').attr('title');

  def.parent().find('li').removeClass('active');
  def.addClass('active');
  def.parents('.dropdown.plainPop.contentFilter').children('a').html(titleD+'<i class="material-icons">arrow_drop_down</i>');
  def.parents('.dropdown.plainPop.contentFilter').children('a').attr('name', tableIDD);
  def.parents('.dropdown.plainPop.contentFilter').children('a').attr('title', titleD);
  $('#task').find('.badgeCircle').text(badgeTextD);
  $('#task .headingtag').text(titleD).attr('data-taskType',titleD);
  $('#task').find('.table.mainTable').hide();
  $('#task').find('#' + tableIDD).show();

  def.parents('.dropdown-contents').find('a').text(titleActualD);
  def.parents('.dropdown-contents').find('a').attr('name',nameActualD);

  setTimeout(function(){window.dispatchEvent(new Event('resize'));}, 200);


$('#task .filterTrigger .dropdown-contents li').off('click').on('click', function(e){
  e.preventDefault();
  var self = $(this).find('input[type="checkbox"]');
  if(self.is(':checked')){
    self.prop('checked', false);
  }
  else{
    self.prop('checked', true);
  }
  var val = self.val();
  // alert(self.is(':checked'));
  if(self.is(':checked')){
      var template = '<div class="filter-item selected-filters" name="'+val+'"><span class="filter-text">'+val+'</span><a href="#" class="remove-filter"><i class="jdicon jdicon-close"></i></a></div>';
      $('.filter-values').prepend(template);
    }
    else{
      $('.filter-values').find('[name="'+val+'"]').remove();
    }

  filterValueSpace();
})


$(document).on('click', '.filter-item .remove-filter', function(){
  $(this).parents('.selected-filters').remove();
  var val = $(this).parents('.selected-filters').attr('name');

  $('.taskstatus [value="'+val+'"]').prop('checked', false);
  
  filterValueSpace();
})


$(document).on('click', '.clear-filters', function(){

  $('.taskstatus [type="checkbox"]').prop('checked', false);
  $('.filter-values .selected-filters').remove();

  filterValueSpace();
})




});



function updateColor(){
  document.getElementById('color_val').innerHTML = document.getElementById('primary_color').value;
}

function filterValueSpace(){
  setTimeout(
    function(){
      if($('.filter-values .selected-filters').length > 0){
        $('.filter-values').show();
        $('.filter-values').addClass('active');
      }
      else{
        $('.filter-values').removeClass('active');
        $('.filter-values').hide();
      }
      var fvH = $('.filter-values').height();
      console.log(fvH);
      if(!$('.filter-values').hasClass('active')){
        fvH = 0;
      }
      $('#task .content-body').css('margin-top',(118+fvH));
    },250
  )
}

// document.addEventListener('DOMContentLoaded', function() {
//   var elems = document.querySelectorAll('select');
//   var instances = M.FormSelect.init(elems);
// });







/*
$('#task')   //landing page
$('#createTaskWrap')   //plus icon
$('#jobdetails')   //task view/edit
$('#createJobCon')   //create task
$('.detailstask-edit')   //discard changes
$('#pagepoup')   //subtask view/edit
$('#pagepoupPop')   //landing page
$('')   //landing page
$('')   //landing page
$('')   //landing page
$('')   //landing page
$('')   //landing page
$('')   //landing page
$('')   //landing page
*/