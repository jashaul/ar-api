$(function (){
  /* Functions */
  var loadForm = function () {
    /*alert("1");*/
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',      
      beforeSend: function () {
        $("#modal-sim .modal-content").html("");
        $("#modal-sim").modal("show");
      },
      success: function (data) {
        $("#modal-sim .modal-content").html(data.html_form);
      }
    });
  };

var saveForm = function () {
    var form = $(this);
    var url= form.attr("action");
    var type= form.attr("method");
    var srldata= form.serialize();
    var runmode = $('#id_run_mode').val();
    var startyear = this.startyear;
    var endyear = this.endyear;
    var calcstartyear = this.calcstartyear;
    var futcalcyears = this.futcalcyears;
      if (runmode == 'future')
          {            
            if (startyear.value != '' || endyear.value != '')
            {
             alert('For future Start year and endyear should be blank');
             return false;
           }
           if (calcstartyear.value == '' || futcalcyears.value == '')
            {
             alert('For future calcstartyear and futcalcyears should not be blank');
             return false;
           }
          }
      else {
             if (calcstartyear.value != '' || futcalcyears.value != '')
             {
             alert('For history Calc Start year and future calc year should be blank');
             return false;
             }
             if (startyear.value == '' || endyear.value == '')
            {
             alert('For history Start year and endyear should not be blank');
             return false;
           }
      }

    $.ajax({
      url: url,
      data: srldata,
      type: type,
      cache: false,
      dataType: 'json',
      success: function (data) {
        if (data.form_is_valid) {
          alert("3");
          $("#modal-sim").modal("hide");
          }
      }
    });
  };


/* Functions */
  var loadQlik = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',      
      beforeSend: function () {
        $("#modal-qlik .modal-content").html("");
        $("#modal-qlik").modal("show");
      },
      success: function (data) {
        $("#modal-qlik .modal-content").html(data.html_form);
      }
    });
  };


 $("[data-toggle=tooltip]").tooltip();

  /* Binding */

  //select simulation
  //  $(".js-select-sim").click(loadForm);

$("#sim-table").on("click", ".js-select-sim", loadForm);
$("#modal-sim").on("submit", ".js-select-sim-form", saveForm);

  $("#sim-table").on("click", ".js-delete-sim", loadForm);

   $("#sim-table").on("click", ".js-download-sim", loadForm);

   $("#qlik-btn").on("click", ".js-show-qlik", loadQlik);

});
